
import torch
import numpy as np
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torchvision.datasets as datasets
from torch import nn


CONV_ARCH = ((1, 64), (1, 128), (2, 256), (2, 512), (2, 512))

def imshow(img):
	npimg = img.numpy()
	plt.imshow(np.transpose(npimg, (1, 2, 0)))
	plt.show()


def vgg_block(num_convs, in_channels, out_channels):
	layers = []
	for _ in range(num_convs):
		layers.append(nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1))
		layers.append(nn.BatchNorm2d(out_channels))
		layers.append(nn.ReLU())
		in_channels = out_channels
	layers.append(nn.MaxPool2d(kernel_size=2, stride=2))
	return nn.Sequential(*layers)

def vgg():
	conv_blks = []
	in_channels = 3
	# The convolutional part
	for (num_convs, out_channels) in CONV_ARCH:
		conv_blks.append(vgg_block(num_convs, in_channels, out_channels))
		in_channels = out_channels

	return nn.Sequential(
		*conv_blks, nn.Flatten(),
		# The fully-connected part
		nn.Linear(out_channels * 7 * 7, 4096), nn.ReLU(), nn.Dropout(0.5),
		nn.Linear(4096, 4096), nn.ReLU(), nn.Dropout(0.5),
		nn.Linear(4096, 10))


def main():

	print('loop')
	transform = transforms.Compose([
		transforms.Resize(224),
		#transforms.Grayscale(num_output_channels = 1),
		transforms.ToTensor(),
	])

	batch_size = 18

	data_dir = './train_images'
	image_datasets = datasets.ImageFolder(data_dir, transform=transform)
	data_loader = torch.utils.data.DataLoader(image_datasets, batch_size=batch_size, shuffle=True, num_workers=1)

	print(image_datasets)

	classes = ('free', 'full')
	# get some random training images
	dataiter = iter(data_loader)
	images, labels = dataiter.next()

	print(' '.join('%5s' % classes[labels[j]] for j in range(batch_size)))
	#?imshow(torchvision.utils.make_grid(images)) #DEBUG
	

	print('CREATE NEURAL NETWORK')#! CREATE NEURAL NETWORK
	net = vgg()


	print('LOSS FUNCTION & OPTIMIZER')#! LOSS FUNCTION & OPTIMIZER
	criterion = nn.CrossEntropyLoss()
	optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


	print('TRAINING NETWORK')#! TRAINING NETWORK
	for epoch in range(1):  # loop over the dataset multiple times
		running_loss = 0.0
		for i, data in enumerate(data_loader, 0):
			# get the inputs; data is a list of [inputs, labels]
			inputs, labels = data[0], data[1]

			# zero the parameter gradients
			optimizer.zero_grad()

			# forward + backward + optimize
			outputs = net(inputs)
			loss = criterion(outputs, labels)
			loss.backward()
			optimizer.step()

			# print statistics
			running_loss += loss.item()
			if i % 20 == 19:    # print every 2000 mini-batches
				print('[%d, %5d] loss: %.3f' %
					(epoch + 1, i + 1, running_loss / 20))
				running_loss = 0.0
				
	torch.save(net, './parking_VGG.pth')
	print('Finished Training')
	




if __name__ == '__main__':
	main()

