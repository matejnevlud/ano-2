import pathlib

import torch
import numpy as np
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torchvision.datasets as datasets

from keras.utils.vis_utils import plot_model
import tensorflow as tf
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras import Sequential, Input
from tensorflow.keras.layers import Conv2D, AvgPool2D, Flatten, Dense, MaxPool2D, Dropout, ReLU
from keras.preprocessing.image_dataset import image_dataset_from_directory


FILENAME = 'lenet'
IMG_SIZE = (32, 32)
NUM_OF_CLASSES = 2
BATCH_SIZE = 8


def lenet():
    return Sequential([
        Conv2D(filters=6, kernel_size=5, padding='same', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 3)),
        ReLU(),
        AvgPool2D(pool_size=2, strides=2),

        Conv2D(filters=16, kernel_size=5),
        ReLU(),
        AvgPool2D(pool_size=2, strides=2),

        Flatten(),

        Dense(120),
        ReLU(),
        Dense(84),
        ReLU(),
        Dense(NUM_OF_CLASSES, activation='softmax')
    ])


def imshow(img):
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def loadParkingData():
    data_dir = pathlib.Path('./train_images')
    train_ds = image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        labels="inferred",
        label_mode="categorical",
        subset="training",
        seed=123,
        image_size=IMG_SIZE,
        batch_size=BATCH_SIZE)

    val_ds = image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        labels="inferred",
        label_mode="categorical",
        subset="validation",
        seed=123,
        image_size=IMG_SIZE,
        batch_size=BATCH_SIZE)

    global NUM_OF_CLASSES
    NUM_OF_CLASSES = len(train_ds.class_names)
    print("Dataset classes: " + str(train_ds.class_names))

    for image_batch, label_batch in train_ds.take(1):
        print("Image format", image_batch[0].numpy().shape)
        print("Label format", label_batch[0].numpy())



    normalization_layer = tf.keras.layers.Rescaling(1. / 255)
    train_ds_norm = train_ds.map(lambda x, y: (normalization_layer(x), y))
    val_ds_norm = val_ds.map(lambda x, y: (normalization_layer(x), y))

    return train_ds_norm, val_ds_norm


def main():
    train, val = loadParkingData()

    print('CREATE NEURAL NETWORK')  # ! CREATE NEURAL NETWORK

    model = lenet()
    model.compile(
        optimizer=SGD(learning_rate=0.001, momentum=0.9),
        loss='categorical_crossentropy',
        metrics=['accuracy'])

    # plot_model(model, FILENAME + ".png", show_shapes=True)

    history = model.fit(
        train,
        validation_data=val,
        epochs=10
    )

    model.save("./models_tf/" + FILENAME)

    img = tf.keras.utils.load_img(
        './train_images/full/full_152.png', target_size=IMG_SIZE
    )
    img_array = tf.keras.utils.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0)  # Create a batch

    predictions = model.predict(img_array)
    predict_label = np.argmax(predictions[0])
    class_names = ['free', 'full']
    print(
        "This image most likely belongs to {} with a {:.2f} percent confidence."
            .format(class_names[predict_label], 100 * np.max(predictions[0]))
    )

    for image_batch, label_batch in train.take(1):
        i = 0
        for image in image_batch:
            print("Image shape: ", image.numpy().shape)
            img_array = tf.expand_dims(image, 0)  # Create a batch
            predictions = model.predict(img_array)
            predict_label = np.argmax(predictions[0])
            class_names = ['free', 'full']
            print(
                "Predicted label {} with a {:.2f} percent confidence but truth is {} [{}]"
                    .format(class_names[predict_label], 100 * np.max(predictions[0]), class_names[np.argmax(label_batch[i])], predictions)
            )
            i += 1


if __name__ == '__main__':
    main()
