# Výsledky pokusů

| Tables   |      Are      |  Cool |
| -------- | :-----------: | ----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

### PyTorch
| Název pokusu              | F1-score | Accuracy |  Time  |
|---------------------------|:--------:|:--------:| :----: |
| Single threshold          |  0,955   |          |        |
| Multiple threshold        |  0,985   |          |        |
| HOG                       |  0,842   |  87,92   |        |
| HOG (16, 16)              |  0,878   |          |        |
| HOG + LBP prep            |    -     |          |        |
| LBPH_Face                 |  0,934   |  96,03   | ~8 min |
| Basic LeNet               |  0,716   |  75,07   |        |
| AlexNet 10 class 5 epoch  |  0,859   |  89,73   |        |
| AlexNet 2 classes 5 epoch |    -     |    -     |        |
| VGG                       |    -     |    -     |        |

### Tensorflow
Každá síť byla trénována 5 / 10 epoch a optimalizována pomocí Adam(lr=1e-3) nebo SGD(lr=1e-3, momentum=0.9)

| Název sítě | Train accuracy | F1-score | Real Accuracy |  Time  | Notes                             |
|------------|:--------------:|:--------:|:-------------:|:------:|:----------------------------------|
| LeNet      |     96.34      |   0.91   |     94.17     |  21s   | Adam, Categorical                 |
| LeNet      |     92.12      |   0.43   |     89.15     |  22s   | Adam, Binary                      |
| AlexNet    |     97.33      |   0.95   |     97.20     | 3m 35s | SGD, Categorical                  |
| VGG        |     98.78      |   0.91   |     94.79     | 8m 24s | SGD, Categorical                  |
| GoogleNet  |     98.83      |   0.75   |     78.86     | 6m 12s | Adam, Categorical                 |
| GoogleNet  |     99.64      |   0.81   |     85.86     | 6m 12s | Adam, Binary, 10                  |
| GoogleNet* |     99.90      |   0.18   |     66.54     | 2m 42s | SGD, Categorical, Pretrained      |
| GoogleNet* |     99.90      |   0.51   |     73.73     | 2m 42s | SGD, Binary, Pretrained           |
| GoogleNet* |     99.90      |   0.80   |     88.46     | 2m 42s | Adam(1e-4), Binary, Pretrained, 5 |
| ResNet*    |     73.43      |   0.28   |     42.46     | 2m 10s | SGD, Binary, Pretrained, 5        |
| ResNet*    |     73.43      |   0.28   |     42.41     | 2m 10s | SGD, Binary, Pretrained, 10       |


### GoogLeNet

#### Naměřené výsledky
| Počet epoch | Nákladová funkce | Optimalizační funkce | Trénovací přesnost | F1-score | Testovací přesnost | Čas trénování |
|-------------|------------------|----------------------|:------------------:|:--------:|:------------------:|:-------------:|
| 5           | Binary           | SGD(η = 1e-3, m=0.9) |       81.93        |   0.48   |       31.69        |    3m 49s     |
| 5           | Binary           | SGD(η = 1e-4, m=0.9) |       81.93        |   0.48   |       31.69        |    3m 49s     |
| 5           | Binary           | Adam(η = 1e-3)       |       97.48        |   0.48   |       31.69        |     4m 4s     |
| 5           | Binary           | Adam(η = 1e-4)       |       99.33        |   0.79   |       84.07        |    3m 52s     |
| 10          | Binary           | Adam(η = 1e-3)       |       98.32        |   0.48   |       31.69        |    7m 10s     |
| 10          | Binary           | Adam(η = 1e-4)       |       98.32        |   0.48   |       31.69        |    7m 10s     |

#### Naměřené výsledky - předtrénovaná síť
| Název sítě | Train accuracy | F1-score | Real Accuracy |  Time  | Notes                             |
|------------|:--------------:|:--------:|:-------------:|:------:|:----------------------------------|
| GoogleNet* |     99.90      |   0.18   |     66.54     | 2m 42s | SGD, Categorical, Pretrained      |
| GoogleNet* |     99.90      |   0.51   |     73.73     | 2m 42s | SGD, Binary, Pretrained           |
| GoogleNet* |     99.90      |   0.80   |     88.46     | 2m 42s | Adam(1e-4), Binary, Pretrained, 5 |
