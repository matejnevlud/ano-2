
import torch
import numpy as np
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torchvision.datasets as datasets
from torch import nn

def imshow(img):
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()





def main():

    print('loop')
    transform = transforms.Compose([
        transforms.Resize(32),
        #transforms.Grayscale(num_output_channels = 1),
        transforms.ToTensor(),
    ])

    batch_size = 8

    data_dir = './train_images'
    image_datasets = datasets.ImageFolder(data_dir, transform=transform)
    data_loader = torch.utils.data.DataLoader(image_datasets, batch_size=batch_size, shuffle=True, num_workers=1)

    print(image_datasets)

    classes = ('free', 'full')
    # get some random training images
    dataiter = iter(data_loader)
    images, labels = dataiter.next()

    print(' '.join('%5s' % classes[labels[j]] for j in range(batch_size)))
    #?imshow(torchvision.utils.make_grid(images)) #DEBUG
    

    print('CREATE NEURAL NETWORK')#! CREATE NEURAL NETWORK
    net = nn.Sequential(
        nn.Conv2d(3, 6, kernel_size=5, padding=0, stride=1),
        nn.ReLU(),
        nn.AvgPool2d(kernel_size=2, stride=2),

        nn.Conv2d(6, 16, kernel_size=5, padding=0, stride=1),
        nn.ReLU(),
        nn.AvgPool2d(kernel_size=2, stride=2),

        nn.Flatten(),

        nn.Linear(in_features=16 * 5 * 5, out_features=120),
        nn.ReLU(),

        nn.Linear(in_features=120, out_features=84),
        nn.ReLU(),

        nn.Linear(in_features=84, out_features=2),
    )


    print('LOSS FUNCTION & OPTIMIZER')#! LOSS FUNCTION & OPTIMIZER
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


    print('TRAINING NETWORK')#! TRAINING NETWORK
    for epoch in range(10):  # loop over the dataset multiple times
        running_loss = 0.0
        for i, data in enumerate(data_loader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data[0], data[1]

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 20 == 19:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                    (epoch + 1, i + 1, running_loss / 20))
                running_loss = 0.0
    torch.save(net, './parking_lenet.pth')
    print('Finished Training')
    




if __name__ == '__main__':
    main()

