import os
import pathlib
import sys
import random
from timeit import default_timer as timer
import keras.losses
import torch
import numpy as np
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torchvision.datasets as datasets
from PIL import Image, ImageEnhance, ImageOps
from keras import Model
from keras.applications.densenet import DenseNet121
from keras.applications.inception_v3 import InceptionV3
from keras.applications.resnet import ResNet101, ResNet50
from keras.applications.vgg16 import VGG16
from keras.layers import Rescaling, RandomRotation, RandomContrast, RandomFlip, GlobalAveragePooling2D, AveragePooling2D
from keras.losses import BinaryCrossentropy

from keras.utils.vis_utils import plot_model
import tensorflow as tf
from tensorflow.core.protobuf.config_pb2 import ConfigProto
from tensorflow.keras.optimizers import SGD, Adam, Adagrad

from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras import Sequential, Input
from tensorflow.keras.layers import Conv2D, AvgPool2D, Flatten, Dense, MaxPool2D, Dropout, ReLU, BatchNormalization, Concatenate, GlobalAvgPool2D, Activation, Layer
from keras.preprocessing.image_dataset import image_dataset_from_directory
from tensorflow.python.client.session import Session

from test_net_tf import test_net

# !##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!#!#
AUGMENTED = False
EPOCHS = 10
OPTIMIZER = SGD(learning_rate=1e-3, momentum=0.9)
# !##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!##!#!#


IMG_SIZE = (224, 224)
BATCH_SIZE = 32
BINARY_CROSSENTROPY = False
TRAINABLE = True
NUM_OF_CLASSES = 1 if BINARY_CROSSENTROPY else 2


def lenet():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    x = Rescaling(scale=1 / 255.0)(input)

    x = Conv2D(filters=6, kernel_size=5, padding='same', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 3))(x)
    x = ReLU()(x)
    x = AvgPool2D(pool_size=2, strides=2)(x)

    x = Conv2D(filters=16, kernel_size=5)(x)
    x = ReLU()(x)
    x = AvgPool2D(pool_size=2, strides=2)(x)

    x = Flatten()(x)

    x = Dense(120)(x)
    x = ReLU()(x)
    x = Dense(84)(x)
    x = ReLU()(x)

    x = Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')(x)

    return Model(input, x)


def alexnet():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    x = Rescaling(scale=1 / 255.0)(input)

    x = Conv2D(filters=96, kernel_size=11, strides=4, activation='relu', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 3))(x)
    x = MaxPool2D(pool_size=3, strides=2)(x)

    x = Conv2D(filters=256, kernel_size=5, padding='same', activation='relu')(x)
    x = MaxPool2D(pool_size=3, strides=2)(x)

    x = Conv2D(filters=384, kernel_size=3, padding='same', activation='relu')(x)
    x = Conv2D(filters=384, kernel_size=3, padding='same', activation='relu')(x)
    x = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(x)
    x = MaxPool2D(pool_size=3, strides=2)(x)
    x = Flatten()(x)

    x = Dense(4096, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(4096, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')(x)

    return Model(input, x)


def vgg():
    def vgg_block_batch_norm(num_convs, num_channels):
        layers_block = tf.keras.models.Sequential()
        for _ in range(num_convs):
            layers_block.add(Conv2D(num_channels, kernel_size=3, padding='same'))
            layers_block.add(BatchNormalization())
            layers_block.add(ReLU())
        layers_block.add(MaxPool2D(pool_size=2, strides=2))
        return layers_block

    def vgg_block(num_convs, num_channels):
        layers_block = tf.keras.models.Sequential()
        for _ in range(num_convs):
            layers_block.add(Conv2D(num_channels, kernel_size=3, padding='same', activation='relu'))
        layers_block.add(MaxPool2D(pool_size=2, strides=2))
        return layers_block

    conv_arch = ((1, 64), (1, 128), (2, 256), (2, 512), (2, 512))

    model = tf.keras.models.Sequential()

    # The convulational part
    for (num_convs, num_channels) in conv_arch:
        model.add(vgg_block(num_convs, num_channels))
    # The fully-connected part
    model.add(tf.keras.models.Sequential([
        Flatten(),
        Dense(4096, activation='relu'),
        Dropout(0.5),
        Dense(4096, activation='relu'),
        Dropout(0.5),
        Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')]))

    return model


def inception():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    input = Rescaling(scale=1 / 255.0)(input)

    return InceptionV3(include_top=True, input_tensor=input, weights=None, classes=NUM_OF_CLASSES, classifier_activation='softmax')

    class Inception(tf.keras.Model):
        # `c1`--`c4` are the number of output channels for each path
        def __init__(self, c1, c2, c3, c4):
            super().__init__()
            # Path 1 is a single 1 x 1 convolutional layer
            self.p1_1 = Conv2D(c1, 1, activation='relu')
            # Path 2 is a 1 x 1 convolutional layer followed by a 3 x 3
            # convolutional layer
            self.p2_1 = Conv2D(c2[0], 1, activation='relu')
            self.p2_2 = Conv2D(c2[1], 3, padding='same',
                               activation='relu')
            # Path 3 is a 1 x 1 convolutional layer followed by a 5 x 5
            # convolutional layer
            self.p3_1 = Conv2D(c3[0], 1, activation='relu')
            self.p3_2 = Conv2D(c3[1], 5, padding='same',
                               activation='relu')
            # Path 4 is a 3 x 3 maximum pooling layer followed by a 1 x 1
            # convolutional layer
            self.p4_1 = MaxPool2D(3, 1, padding='same')
            self.p4_2 = Conv2D(c4, 1, activation='relu')

        def call(self, x):
            p1 = self.p1_1(x)
            p2 = self.p2_2(self.p2_1(x))
            p3 = self.p3_2(self.p3_1(x))
            p4 = self.p4_2(self.p4_1(x))
            # Concatenate the outputs on the channel dimension
            return Concatenate()([p1, p2, p3, p4])

    def b1():
        return tf.keras.models.Sequential([
            Conv2D(64, 7, strides=2, padding='same', activation='relu'),
            MaxPool2D(pool_size=3, strides=2, padding='same')])

    def b2():
        return tf.keras.Sequential([
            Conv2D(64, 1, activation='relu'),
            Conv2D(192, 3, padding='same', activation='relu'),
            MaxPool2D(pool_size=3, strides=2, padding='same')])

    def b3():
        return tf.keras.models.Sequential([
            Inception(64, (96, 128), (16, 32), 32),
            Inception(128, (128, 192), (32, 96), 64),
            MaxPool2D(pool_size=3, strides=2, padding='same')])

    def b4():
        return tf.keras.Sequential([
            Inception(192, (96, 208), (16, 48), 64),
            Inception(160, (112, 224), (24, 64), 64),
            Inception(128, (128, 256), (24, 64), 64),
            Inception(112, (144, 288), (32, 64), 64),
            Inception(256, (160, 320), (32, 128), 128),
            MaxPool2D(pool_size=3, strides=2, padding='same')])

    def b5():
        return tf.keras.Sequential([
            Inception(256, (160, 320), (32, 128), 128),
            Inception(384, (192, 384), (48, 128), 128),
            GlobalAvgPool2D(),
            Flatten()
        ])

    # Recall that this has to be a function that will be passed to
    # `d2l.train_ch6()` so that model building/compiling need to be within
    # `strategy.scope()` in order to utilize the CPU/GPU devices that we have

    return tf.keras.Sequential([b1(), b2(), b3(), b4(), b5(), Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')])


def inception_pre():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    x = Rescaling(scale=1 / 255.0)(input)

    base_model = InceptionV3(include_top=False, weights="imagenet")
    base_model.trainable = False
    x = base_model(x)

    x = AveragePooling2D()(x)
    x = Flatten()(x)

    outputs = Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')(x)
    return Model(input, outputs)


def resnet():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    input = Rescaling(scale=1 / 255.0)(input)

    return ResNet50(include_top=True, input_tensor=input, weights=None, classes=NUM_OF_CLASSES, classifier_activation='softmax')

    class Residual(tf.keras.Model):  # @save
        """The Residual block of ResNet."""

        def __init__(self, num_channels, use_1x1conv=False, strides=1):
            super().__init__()
            self.conv1 = Conv2D(
                num_channels, padding='same', kernel_size=3, strides=strides)
            self.conv2 = Conv2D(
                num_channels, kernel_size=3, padding='same')
            self.conv3 = None
            if use_1x1conv:
                self.conv3 = Conv2D(
                    num_channels, kernel_size=1, strides=strides)
            self.bn1 = BatchNormalization()
            self.bn2 = BatchNormalization()

        def call(self, X):
            Y = tf.keras.activations.relu(self.bn1(self.conv1(X)))
            Y = self.bn2(self.conv2(Y))
            if self.conv3 is not None:
                X = self.conv3(X)
            Y += X
            return tf.keras.activations.relu(Y)

    class ResnetBlock(Layer):
        def __init__(self, num_channels, num_residuals, first_block=False,
                     **kwargs):
            super(ResnetBlock, self).__init__(**kwargs)
            self.residual_layers = []
            for i in range(num_residuals):
                if i == 0 and not first_block:
                    self.residual_layers.append(
                        Residual(num_channels, use_1x1conv=True, strides=2))
                else:
                    self.residual_layers.append(Residual(num_channels))

        def call(self, X):
            for layer in self.residual_layers.layers:
                X = layer(X)
            return X

    return tf.keras.Sequential([
        Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3)),
        Rescaling(scale=1 / 255.0),
        # The following layers are the same as b1 that we created earlier
        Conv2D(64, kernel_size=7, strides=2, padding='same'),
        BatchNormalization(),
        Activation('relu'),
        MaxPool2D(pool_size=3, strides=2, padding='same'),
        # The following layers are the same as b2, b3, b4, and b5 that we
        # created earlier
        ResnetBlock(64, 2, first_block=True),
        ResnetBlock(128, 2),
        ResnetBlock(256, 2),
        ResnetBlock(512, 2),
        GlobalAvgPool2D(),
        Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')])


def resnet_pre():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    x = Rescaling(scale=1 / 255.0)(input)

    base_model = ResNet101(include_top=False, weights="imagenet")
    base_model.trainable = False
    x = base_model(x)

    x = AveragePooling2D()(x)
    x = Flatten()(x)

    outputs = Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')(x)
    return Model(input, outputs)


def densenet():
    input = Input(shape=(IMG_SIZE[0], IMG_SIZE[1], 3))
    x = Rescaling(scale=1 / 255.0)(input)

    base_model = DenseNet121(
        include_top=False,
        weights="imagenet",
    )
    base_model.trainable = False
    x = base_model(x, training=False)

    x = AveragePooling2D()(x)
    x = Flatten()(x)

    outputs = Dense(NUM_OF_CLASSES, activation='sigmoid' if BINARY_CROSSENTROPY else 'softmax')(x)
    return Model(input, outputs)


def determineNeuralNet(neuralNetName):
    if neuralNetName == 'lenet':
        global IMG_SIZE
        IMG_SIZE = (32, 32)
        return lenet()
    elif neuralNetName == 'alexnet':
        return alexnet()
    elif neuralNetName == 'vgg':
        return vgg()
    elif neuralNetName == 'inception':
        return inception()
    elif neuralNetName == 'inception_pre':
        return inception_pre()
    elif neuralNetName == 'resnet':
        return resnet()
    elif neuralNetName == 'resnet_pre':
        return resnet_pre()
    elif neuralNetName == 'densenet':
        return densenet()


def imshow(img):
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def augment():
    labels = ['free', 'full']

    if not os.path.exists('./augment_images'):
        os.mkdir('./augment_images')

    for label in labels:
        if not os.path.exists('./augment_images/' + label):
            os.mkdir('./augment_images/' + label)

        file_names = os.listdir('./train_images/' + label)
        for file in file_names:
            file_wo_type = file.split('.')[0]
            original_img = Image.open('./train_images/' + label + '/' + file)
            original_img.save('./augment_images/' + label + '/' + file_wo_type + '.png', "PNG")
            for i in range(3):
                brightness_filter = ImageEnhance.Brightness(original_img)
                brightness_img = brightness_filter.enhance(random.uniform(0.4, 1.2))
                brightness_img.save('./augment_images/' + label + '/' + file_wo_type + '_' + str(i) + '.png', "PNG")
                if random.uniform(0.0, 1.0) > 0.5:
                    im_mirror = ImageOps.mirror(brightness_img)
                    im_mirror.save('./augment_images/' + label + '/' + file_wo_type + '_' + 'flip' + '.png', "PNG")


def loadParkingData():
    data_dir = pathlib.Path('./train_images')
    data_dir = pathlib.Path('./augment_images') if AUGMENTED else pathlib.Path('./train_images')
    train_ds = image_dataset_from_directory(
        data_dir,
        labels="inferred",
        label_mode='binary' if BINARY_CROSSENTROPY else 'categorical',
        seed=123,
        image_size=IMG_SIZE,
        batch_size=BATCH_SIZE)

    val_ds = image_dataset_from_directory(
        pathlib.Path('./validation_images'),
        labels="inferred",
        label_mode='binary' if BINARY_CROSSENTROPY else 'categorical',
        seed=123,
        image_size=IMG_SIZE,
        batch_size=BATCH_SIZE)

    global NUM_OF_CLASSES
    NUM_OF_CLASSES = len(train_ds.class_names)
    print("Dataset classes: " + str(train_ds.class_names))

    for image_batch, label_batch in train_ds.take(1):
        print("Image format", image_batch[0].numpy().shape)
        print("Label format", label_batch[0].numpy())

    normalization_layer = Rescaling(1. / 255)
    # train_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
    # val_ds = val_ds.map(lambda x, y: (normalization_layer(x), y))

    return train_ds, val_ds


def trainNetwork(neuralNetName):
    print('CREATE NEURAL NETWORK', neuralNetName)  # ! CREATE NEURAL NETWORK

    model = determineNeuralNet(neuralNetName)
    model.compile(
        optimizer=OPTIMIZER,
        loss=keras.losses.BinaryCrossentropy() if BINARY_CROSSENTROPY else keras.losses.CategoricalCrossentropy(),
        metrics=['acc'])

    # plot_model(model, neuralNetName + ".png", show_shapes=True)

    train, val = loadParkingData()

    history = model.fit(
        train,
        validation_data=val,
        epochs=EPOCHS
    )

    model.save("./models_tf/" + neuralNetName)

    for image_batch, label_batch in train.take(1):
        i = 0
        for image in image_batch:
            img_array = tf.expand_dims(image, 0)  # Create a batch
            predictions = model.predict(img_array)
            predict_label_idx = np.where(predictions[0] > 0.5, 1, 0)[0] if BINARY_CROSSENTROPY else np.argmax(predictions[0])

            class_names = ['free', 'full']
            predict_label = class_names[predict_label_idx]
            truth_label = label_batch[i] if BINARY_CROSSENTROPY else np.argmax(label_batch[i])

            print("Predicted {} with a {:.2f} confidence - truth is {} [{}]"
                  .format(predict_label, 100 * np.max(predictions[0]), truth_label, predictions)
                  )
            i += 1

    # print(neuralNetName, EPOCHS, 'binary' if BINARY_CROSSENTROPY else 'categorical', 'SGD' if isinstance(OPTIMIZER, SGD) else 'Adam', 'lr=' + str(OPTIMIZER.get_config()['learning_rate']), history.history['acc'][EPOCHS-1])
    summary = neuralNetName + ';'
    summary += str(EPOCHS) + ';'
    summary += 'binary;' if BINARY_CROSSENTROPY else 'categorical;'
    summary += 'SGD;' if isinstance(OPTIMIZER, SGD) else 'Adam;'
    summary += str(OPTIMIZER.get_config()['learning_rate']) + ';'
    summary += str(history.history['acc'][EPOCHS - 1]) + ';'

    print(summary)

    return summary


def benchmark(TEST_NET):
    global EPOCHS
    global AUGMENTED
    global OPTIMIZER


    EPOCHS = 5
    AUGMENTED = False
    start = timer()
    train_summary = trainNetwork('resnet')
    end = timer()
    test_summary = test_net('resnet')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()


    EPOCHS = 10
    AUGMENTED = False
    start = timer()
    train_summary = trainNetwork('resnet')
    end = timer()
    test_summary = test_net('resnet')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    EPOCHS = 5
    AUGMENTED = True
    start = timer()
    train_summary = trainNetwork('resnet')
    end = timer()
    test_summary = test_net('resnet')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    EPOCHS = 10
    AUGMENTED = True
    start = timer()
    train_summary = trainNetwork('resnet')
    end = timer()
    test_summary = test_net('resnet')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    #### RESNETPRE
    OPTIMIZER = SGD(learning_rate=1e-4, momentum=0.9)

    EPOCHS = 5
    AUGMENTED = False
    start = timer()
    train_summary = trainNetwork('resnet_pre')
    end = timer()
    test_summary = test_net('resnet_pre')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    EPOCHS = 10
    AUGMENTED = False
    start = timer()
    train_summary = trainNetwork('resnet_pre')
    end = timer()
    test_summary = test_net('resnet_pre')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    EPOCHS = 5
    AUGMENTED = True
    start = timer()
    train_summary = trainNetwork('resnet_pre')
    end = timer()
    test_summary = test_net('resnet_pre')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()

    EPOCHS = 10
    AUGMENTED = True
    start = timer()
    train_summary = trainNetwork('resnet_pre')
    end = timer()
    test_summary = test_net('resnet_pre')

    line = train_summary + ';' + test_summary + ';' + str(end - start)
    print(line)
    f = open("benchmark.txt", "a")
    f.write(line + '\n')
    f.close()



def main(neuralNetName):
    trainNetwork(neuralNetName)


if __name__ == '__main__':
    if not sys.argv[1:][0]:
        print("Please specify neural net")
    elif sys.argv[1:][0] == 'benchmark':
        benchmark(sys.argv[1:][1])
    elif sys.argv[1:][0] == 'augment':
        augment()
    else:
        main(sys.argv[1:][0])
