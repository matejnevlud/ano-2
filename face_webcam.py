import cv2
import test_images
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())


while cap.isOpened():
	ret, opencv_frame = cap.read()
	gray = cv2.cvtColor(opencv_frame, cv2.COLOR_BGR2GRAY)

	#faces = face_cascade.detectMultiScale(gray, 1.1, 0)
	(rects, weights) = hog.detectMultiScale(gray, winStride=(4, 4), padding=(0, 0), scale=1.05)

	for (x, y, w, h) in rects:
		cv2.rectangle(opencv_frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

	cv2.imshow("opencv_frame", opencv_frame)
	cv2.waitKey(2)