
import torch
import numpy as np
import scipy
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torchvision.datasets as datasets
from torch import nn
from torchvision import models
from torch.nn import functional as F

import googlenet

CONV_ARCH = ((1, 64), (1, 128), (2, 256), (2, 512), (2, 512))

def imshow(img):
	npimg = img.numpy()
	plt.imshow(np.transpose(npimg, (1, 2, 0)))
	plt.show()


def vgg_block(num_convs, in_channels, out_channels):
	layers = []
	for _ in range(num_convs):
		layers.append(nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1))
		layers.append(nn.BatchNorm2d(out_channels))
		layers.append(nn.ReLU())
		in_channels = out_channels
	layers.append(nn.MaxPool2d(kernel_size=2, stride=2))
	return nn.Sequential(*layers)

def vgg():
	conv_blks = []
	in_channels = 3
	# The convolutional part
	for (num_convs, out_channels) in CONV_ARCH:
		conv_blks.append(vgg_block(num_convs, in_channels, out_channels))
		in_channels = out_channels

	return nn.Sequential(
		*conv_blks, nn.Flatten(),
		# The fully-connected part
		nn.Linear(out_channels * 7 * 7, 4096), nn.ReLU(), nn.Dropout(0.5),
		nn.Linear(4096, 4096), nn.ReLU(), nn.Dropout(0.5),
		nn.Linear(4096, 10))


class InceptionBATCHNORM(nn.Module):
	# `c1`--`c4` are the number of output channels for each path
	def __init__(self, in_channels, c1, c2, c3, c4, **kwargs):
		super(Inception, self).__init__(**kwargs)
		# Path 1 is a single 1 x 1 convolutional layer
		self.p1_1 = nn.Conv2d(in_channels, c1, kernel_size=1)
		self.p1_2 = nn.BatchNorm2d(c1)
		# Path 2 is a 1 x 1 convolutional layer followed by a 3 x 3
		# convolutional layer
		self.p2_1 = nn.Conv2d(in_channels, c2[0], kernel_size=1)
		self.p2_2 = nn.BatchNorm2d(c2[0])
		self.p2_3 = nn.Conv2d(c2[0], c2[1], kernel_size=3, padding=1)
		self.p2_4 = nn.BatchNorm2d(c2[1])
		# Path 3 is a 1 x 1 convolutional layer followed by a 5 x 5
		# convolutional layer
		self.p3_1 = nn.Conv2d(in_channels, c3[0], kernel_size=1)
		self.p3_2 = nn.BatchNorm2d(c3[0])
		self.p3_3 = nn.Conv2d(c3[0], c3[1], kernel_size=5, padding=2)
		self.p3_4 = nn.BatchNorm2d(c3[1])
		# Path 4 is a 3 x 3 maximum pooling layer followed by a 1 x 1
		# convolutional layer
		self.p4_1 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
		self.p4_2 = nn.Conv2d(in_channels, c4, kernel_size=1)
		self.p4_3 = nn.BatchNorm2d(c4)

	def forward(self, x):
		p1 = F.relu(self.p1_2(self.p1_1(x)))

		p2 = F.relu(self.p2_3(self.p2_2(F.relu(self.p2_2(self.p2_1(x))))))

		p3 = F.relu(self.p3_3(self.p3_2(F.relu(self.p3_2(self.p3_1(x))))))

		p4 = F.relu(self.p4_3(self.p4_2(self.p4_1(x))))
		# Concatenate the outputs on the channel dimension
		return torch.cat((p1, p2, p3, p4), dim=1)

class Inception(nn.Module):
    # `c1`--`c4` are the number of output channels for each path
    def __init__(self, in_channels, c1, c2, c3, c4, **kwargs):
        super(Inception, self).__init__(**kwargs)
        # Path 1 is a single 1 x 1 convolutional layer
        self.p1_1 = nn.Conv2d(in_channels, c1, kernel_size=1)
        # Path 2 is a 1 x 1 convolutional layer followed by a 3 x 3
        # convolutional layer
        self.p2_1 = nn.Conv2d(in_channels, c2[0], kernel_size=1)
        self.p2_2 = nn.Conv2d(c2[0], c2[1], kernel_size=3, padding=1)
        # Path 3 is a 1 x 1 convolutional layer followed by a 5 x 5
        # convolutional layer
        self.p3_1 = nn.Conv2d(in_channels, c3[0], kernel_size=1)
        self.p3_2 = nn.Conv2d(c3[0], c3[1], kernel_size=5, padding=2)
        # Path 4 is a 3 x 3 maximum pooling layer followed by a 1 x 1
        # convolutional layer
        self.p4_1 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
        self.p4_2 = nn.Conv2d(in_channels, c4, kernel_size=1)

    def forward(self, x):
        p1 = F.relu(self.p1_1(x))
        p2 = F.relu(self.p2_2(F.relu(self.p2_1(x))))
        p3 = F.relu(self.p3_2(F.relu(self.p3_1(x))))
        p4 = F.relu(self.p4_2(self.p4_1(x)))
        # Concatenate the outputs on the channel dimension
        return torch.cat((p1, p2, p3, p4), dim=1)

def googlenetEVAL():
	m = googlenet.GoogLeNet()
	m.eval()
	print(m)
	return m
	b1 = nn.Sequential(
			nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3),
			nn.ReLU(), nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
	)

	b2 = nn.Sequential(
			nn.Conv2d(64, 64, kernel_size=1), nn.ReLU(),
			nn.Conv2d(64, 192, kernel_size=3, padding=1), nn.ReLU(),
			nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
	)

	b3 = nn.Sequential(
			Inception(192, 64, (96, 128), (16, 32), 32),
			Inception(256, 128, (128, 192), (32, 96), 64),
			nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
	)

	b4 = nn.Sequential(
			Inception(480, 192, (96, 208), (16, 48), 64),
			Inception(512, 160, (112, 224), (24, 64), 64),
			Inception(512, 128, (128, 256), (24, 64), 64),
			Inception(512, 112, (144, 288), (32, 64), 64),
			Inception(528, 256, (160, 320), (32, 128), 128),
			nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
	)

	b5 = nn.Sequential(
			Inception(832, 256, (160, 320), (32, 128), 128),
			Inception(832, 384, (192, 384), (48, 128), 128),
			nn.AdaptiveAvgPool2d((1, 1)), nn.Flatten()
	)

	return nn.Sequential(b1, b2, b3, b4, b5, nn.Linear(1024, 2))


def main():

	print('loop')
	transform = transforms.Compose([
		transforms.Resize(96),
		#transforms.Grayscale(num_output_channels = 1),
		transforms.ToTensor(),
	])

	batch_size = 18

	data_dir = './train_images'
	image_datasets = datasets.ImageFolder(data_dir, transform=transform)
	data_loader = torch.utils.data.DataLoader(image_datasets, batch_size=batch_size, shuffle=True, num_workers=1)

	print(image_datasets)

	classes = ('free', 'full')
	# get some random training images
	dataiter = iter(data_loader)
	images, labels = dataiter.next()

	print(' '.join('%5s' % classes[labels[j]] for j in range(batch_size)))
	#?imshow(torchvision.utils.make_grid(images)) #DEBUG
	

	print('CREATE NEURAL NETWORK')#! CREATE NEURAL NETWORK
	net = googlenetEVAL()


	print('LOSS FUNCTION & OPTIMIZER')#! LOSS FUNCTION & OPTIMIZER
	criterion = nn.CrossEntropyLoss()
	optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


	print('TRAINING NETWORK')#! TRAINING NETWORK
	for epoch in range(1):  # loop over the dataset multiple times
		running_loss = 0.0
		for i, data in enumerate(data_loader, 0):
			# get the inputs; data is a list of [inputs, labels]
			inputs, labels = data[0], data[1]

			# zero the parameter gradients
			optimizer.zero_grad()

			# forward + backward + optimize
			outputs = net(inputs)
			loss = criterion(outputs, labels)
			loss.backward()
			optimizer.step()

			# print statistics
			running_loss += loss.item()
			if i % 20 == 19:    # print every 2000 mini-batches
				print('[%d, %5d] loss: %.3f' %
					(epoch + 1, i + 1, running_loss / 20))
				running_loss = 0.0
				
	torch.save(net, './parking_googlenet.pth')
	print('Finished Training')
	




if __name__ == '__main__':
	main()

