#!/usr/bin/python

import sys
import cv2
import numpy as np
import math
import struct
from datetime import datetime
import glob

import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, exposure
from skimage import io

def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect

def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	rect = order_points(pts)
	(tl, tr, br, bl) = rect
	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))
	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))
	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")
	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(rect, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
	# return the warped image
	return warped


def preprocess_lbp(image, size, radius):
	S = size, size, 1
	gray_mat = np.zeros(S, dtype=np.uint8)
	lbp_mat = np.zeros(S, dtype=np.uint8)
	for y in range(size):
		for x in range(size):
			
			# Convert to gray image
			if isinstance(image[y, x], np.ndarray):
				gray_mat[y, x] = min(int(image[y, x, 0]), min(int(image[y, x, 1]), int(image[y, x, 2]))) 
			else:
				gray_mat[y, x] = int(image[y, x])

			

	return lbp_mat


def main_hog_only(argv):

	cv2.namedWindow("detection", 0)
	cv2.namedWindow('blur_image')
	cv2.namedWindow('res_image') 
	cv2.namedWindow('edge_image')

	pkm_file = open('parking_map_python.txt', 'r')
	pkm_lines = pkm_file.readlines()
	pkm_coordinates = []

	for line in pkm_lines:
		st_line = line.strip()
		sp_line = list(st_line.split(" "))
		pkm_coordinates.append(sp_line)


	train_images_free = [img for img in glob.glob("train_images/free/*.png")]
	train_images_full = [img for img in glob.glob("train_images/full/*.png")]
	
	train_labels_list = []
	train_images_list = []

	IMG_SIZE = 96

	#hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (32, 32), (16, 16), (8,8), 9, 1, -1, 0, 0.2, 1, 64, True)
	hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (16, 16), (8, 8), (4, 4), 9, 1, -1, 0, 0.2, 1, 64, True)
	svm = cv2.ml.SVM_create()
	svm.setType(cv2.ml.SVM_C_SVC)
	svm.setKernel(cv2.ml.SVM_INTER)
	svm.setC(100.0)

	svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-6))



	for i in range(len(train_images_full)):
		one_park_image = cv2.imread(train_images_full[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)
		hog_feature = hog.compute(res_image)
		train_images_list.append(hog_feature)
		train_labels_list.append(1)


	for i in range(len(train_images_free)):
		one_park_image = cv2.imread(train_images_free[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		hog_feature = hog.compute(res_image)
		train_images_list.append(hog_feature)
		train_labels_list.append(0)

	print("Size of training images ", len(train_images_list))


	svm.train(np.array(train_images_list), cv2.ml.ROW_SAMPLE, np.array(train_labels_list))
	print("Train HOG done")



	test_images = [img for img in glob.glob("test_images/*.jpg")]
	test_images.sort()

	gt_file = open("groundtruth.txt", "r")
	gt_lines = gt_file.readlines()
	result_list = []
	current_parking_spot = 0
	
	for img in test_images:
		one_park_image = cv2.imread(img)
		one_img_paint = one_park_image.copy()
		for one_line in pkm_coordinates:
			pts = [((float(one_line[0])), float(one_line[1])),
					((float(one_line[2])), float(one_line[3])),
					((float(one_line[4])), float(one_line[5])),
					((float(one_line[6])), float(one_line[7]))]

			point1 = (int(one_line[0]), int(one_line[1]))
			point2 = (int(one_line[2]), int(one_line[3]))
			point3 = (int(one_line[4]), int(one_line[5]))
			point4 = (int(one_line[6]), int(one_line[7]))

			warped_image = four_point_transform(one_park_image, np.array(pts))
			res_image = cv2.resize(warped_image, (IMG_SIZE, IMG_SIZE))
			gray_image = cv2.cvtColor(res_image, cv2.COLOR_BGR2GRAY)

			hog_feature = hog.compute(gray_image)
			predict_label = svm.predict(np.array(hog_feature).reshape(1, -1))

			
			predict_label = int(predict_label[1][0][0])

			print(predict_label)

			if predict_label == 0:
				#print("Parking cell is empty!")
				result_list.append(0)
				cv2.line(one_park_image, point1, point2, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point3, point4, (255, 0, 0), 4)
				cv2.line(one_park_image, point4, point1, (255, 0, 0), 4)
			else:
				#print("Parking cell is occupied.")
				result_list.append(1)
				cv2.line(one_park_image, point1, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point4, (255, 0, 0), 4)
				#cv2.rectangle(one_park_image, point1, point4, (0, 0, 255))
			
			if result_list[current_parking_spot] == int(gt_lines[current_parking_spot].strip()):
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)
				#cv2.fillPoly(one_park_image, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
			else:
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 0, 255))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)

			current_parking_spot += 1

		continue
		cv2.imshow('one_park_image', one_park_image)

		lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)
		cv2.imshow('lbp_processed', lbp_processed)
		key = cv2.waitKey(0)
		if key == 27: # exit on ESC
			break


	false_positives = 0
	false_negatives = 0
	true_positives = 0
	true_negatives = 0

	i = 0
	for line in gt_lines:
		gt = int(line.strip())
		if( result_list[i] == 1 and gt == 0 ):
			false_positives += 1
		if( result_list[i] == 0 and gt == 1):
			false_negatives += 1
		if( result_list[i] == 1 and gt == 1 ):
			true_positives += 1
		if( result_list[i] == 0 and gt == 0):
			true_negatives += 1

		i += 1


	acc = (float)(true_positives + true_negatives) / (float)(false_positives + false_negatives + true_negatives + true_positives)
	print("Accuracy: ", (float)((int)(acc * 10000.0)) / 100 )

	precision = (float) (true_positives) / (float)(true_positives + false_positives)
	recall = (float) (true_positives) / (float)(true_positives + false_negatives)
	F_one = (2.0 * (float)(precision * recall)) / (float)(precision + recall)

	print("TP", true_positives)
	print("TN", true_negatives)

	print("FP", false_positives)
	print("FN", false_negatives)

	print("precision: ", precision)
	print("recall: ", recall)
	print("F1 Score: ", F_one)
	
	

def main_lbph(argv):

	cv2.namedWindow("detection", 0)
	cv2.namedWindow('blur_image')
	cv2.namedWindow('res_image') 
	cv2.namedWindow('edge_image')

	pkm_file = open('parking_map_python.txt', 'r')
	pkm_lines = pkm_file.readlines()
	pkm_coordinates = []

	for line in pkm_lines:
		st_line = line.strip()
		sp_line = list(st_line.split(" "))
		pkm_coordinates.append(sp_line)


	train_images_free = [img for img in glob.glob("train_images/free/*.png")]
	train_images_full = [img for img in glob.glob("train_images/full/*.png")]
	
	train_labels_list = []
	train_images_list_lbp = []
	train_images_list = []

	IMG_SIZE = 96

	hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (32, 32), (16, 16), (8,8), 9, 1, -1, 0, 0.2, 1, 64, True)
	#hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (16, 16), (8, 8), (4, 4), 9, 1, -1, 0, 0.2, 1, 64, True)
	svm = cv2.ml.SVM_create()
	svm.setType(cv2.ml.SVM_C_SVC)
	svm.setKernel(cv2.ml.SVM_INTER)
	svm.setC(100.0)

	svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-6))



	for i in range(len(train_images_full)):
		one_park_image = cv2.imread(train_images_full[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)
		hog_feature = hog.compute(res_image)
		train_images_list.append(hog_feature)
		train_labels_list.append(1)
		train_images_list_lbp.append(res_image)


	for i in range(len(train_images_free)):
		one_park_image = cv2.imread(train_images_free[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		hog_feature = hog.compute(res_image)
		train_images_list.append(hog_feature)
		train_labels_list.append(0)
		train_images_list_lbp.append(res_image)

	print("Size of training images ", len(train_images_list))

	# LBP INIT
	lbp_recognizer = cv2.face.LBPHFaceRecognizer_create()
	lbp_recognizer.train(train_images_list_lbp, np.array(train_labels_list))
	print("Train LBPHFaceRecognizer done")

	svm.train(np.array(train_images_list), cv2.ml.ROW_SAMPLE, np.array(train_labels_list))
	print("Train HOG done")



	test_images = [img for img in glob.glob("test_images/*.jpg")]
	test_images.sort()

	gt_file = open("groundtruth.txt", "r")
	gt_lines = gt_file.readlines()
	result_list = []
	current_parking_spot = 0
	
	for img in test_images:
		one_park_image = cv2.imread(img)
		one_img_paint = one_park_image.copy()
		for one_line in pkm_coordinates:
			pts = [((float(one_line[0])), float(one_line[1])),
					((float(one_line[2])), float(one_line[3])),
					((float(one_line[4])), float(one_line[5])),
					((float(one_line[6])), float(one_line[7]))]

			point1 = (int(one_line[0]), int(one_line[1]))
			point2 = (int(one_line[2]), int(one_line[3]))
			point3 = (int(one_line[4]), int(one_line[5]))
			point4 = (int(one_line[6]), int(one_line[7]))

			warped_image = four_point_transform(one_park_image, np.array(pts))
			res_image = cv2.resize(warped_image, (IMG_SIZE, IMG_SIZE))
			gray_image = cv2.cvtColor(res_image, cv2.COLOR_BGR2GRAY)

			#hog_feature = hog.compute(gray_image)
			#predict_label = svm.predict(np.array(hog_feature).reshape(1, -1))
			predict_label = lbp_recognizer.predict(gray_image)
			predict_label = predict_label[0]

			print(predict_label)

			if predict_label == 0:
				#print("Parking cell is empty!")
				result_list.append(0)
				cv2.line(one_park_image, point1, point2, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point3, point4, (255, 0, 0), 4)
				cv2.line(one_park_image, point4, point1, (255, 0, 0), 4)
			else:
				#print("Parking cell is occupied.")
				result_list.append(1)
				cv2.line(one_park_image, point1, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point4, (255, 0, 0), 4)
				#cv2.rectangle(one_park_image, point1, point4, (0, 0, 255))
			
			if result_list[current_parking_spot] == int(gt_lines[current_parking_spot].strip()):
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)
				#cv2.fillPoly(one_park_image, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
			else:
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 0, 255))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)

			current_parking_spot += 1

		continue
		cv2.imshow('one_park_image', one_park_image)

		key = cv2.waitKey(0)
		if key == 27: # exit on ESC
			break


	false_positives = 0
	false_negatives = 0
	true_positives = 0
	true_negatives = 0

	i = 0
	for line in gt_lines:
		gt = int(line.strip())
		if( result_list[i] == 1 and gt == 0 ):
			false_positives += 1
		if( result_list[i] == 0 and gt == 1):
			false_negatives += 1
		if( result_list[i] == 1 and gt == 1 ):
			true_positives += 1
		if( result_list[i] == 0 and gt == 0):
			true_negatives += 1

		i += 1


	acc = (float)(true_positives + true_negatives) / (float)(false_positives + false_negatives + true_negatives + true_positives)
	print("Accuracy: ", (float)((int)(acc * 10000.0)) / 100 )

	precision = (float) (true_positives) / (float)(true_positives + false_positives)
	recall = (float) (true_positives) / (float)(true_positives + false_negatives)
	F_one = (2.0 * (float)(precision * recall)) / (float)(precision + recall)

	print("TP", true_positives)
	print("TN", true_negatives)

	print("FP", false_positives)
	print("FN", false_negatives)

	print("precision: ", precision)
	print("recall: ", recall)
	print("F1 Score: ", F_one)




def main(argv):

	cv2.namedWindow("detection", 0)
	cv2.namedWindow('blur_image')
	cv2.namedWindow('res_image') 
	cv2.namedWindow('edge_image')

	pkm_file = open('parking_map_python.txt', 'r')
	pkm_lines = pkm_file.readlines()
	pkm_coordinates = []

	for line in pkm_lines:
		st_line = line.strip()
		sp_line = list(st_line.split(" "))
		pkm_coordinates.append(sp_line)


	train_images_free = [img for img in glob.glob("train_images/free/*.png")]
	train_images_full = [img for img in glob.glob("train_images/full/*.png")]
	
	train_labels_list = []
	train_images_list = []

	IMG_SIZE = 96

	hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (32, 32), (16, 16), (8,8), 9, 1, -1, 0, 0.2, 1, 64, True)
	#hog = cv2.HOGDescriptor((IMG_SIZE, IMG_SIZE), (16, 16), (8, 8), (4, 4), 9, 1, -1, 0, 0.2, 1, 64, True)
	svm = cv2.ml.SVM_create()
	svm.setType(cv2.ml.SVM_C_SVC)
	svm.setKernel(cv2.ml.SVM_INTER)
	svm.setC(100.0)

	svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-6))



	for i in range(len(train_images_full)):
		one_park_image = cv2.imread(train_images_full[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)
		hog_feature = hog.compute(lbp_processed)
		train_images_list.append(hog_feature)
		train_labels_list.append(1)


	for i in range(len(train_images_free)):
		one_park_image = cv2.imread(train_images_free[i], 0)
		res_image = cv2.resize(one_park_image, (IMG_SIZE, IMG_SIZE))
		lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)
		hog_feature = hog.compute(lbp_processed)
		train_images_list.append(hog_feature)
		train_labels_list.append(0)

	print("Size of training images ", len(train_images_list))


	svm.train(np.array(train_images_list), cv2.ml.ROW_SAMPLE, np.array(train_labels_list))
	print("Train HOG done")



	test_images = [img for img in glob.glob("test_images/*.jpg")]
	test_images.sort()

	gt_file = open("groundtruth.txt", "r")
	gt_lines = gt_file.readlines()
	result_list = []
	current_parking_spot = 0
	
	for img in test_images:
		one_park_image = cv2.imread(img)
		one_img_paint = one_park_image.copy()
		for one_line in pkm_coordinates:
			pts = [((float(one_line[0])), float(one_line[1])),
					((float(one_line[2])), float(one_line[3])),
					((float(one_line[4])), float(one_line[5])),
					((float(one_line[6])), float(one_line[7]))]

			point1 = (int(one_line[0]), int(one_line[1]))
			point2 = (int(one_line[2]), int(one_line[3]))
			point3 = (int(one_line[4]), int(one_line[5]))
			point4 = (int(one_line[6]), int(one_line[7]))

			warped_image = four_point_transform(one_park_image, np.array(pts))
			res_image = cv2.resize(warped_image, (IMG_SIZE, IMG_SIZE))
			gray_image = cv2.cvtColor(res_image, cv2.COLOR_RGB2GRAY)
			lbp_processed = preprocess_lbp(res_image, IMG_SIZE, 4)

			hog_feature = hog.compute(lbp_processed)
			predict_label = svm.predict(np.array(hog_feature).reshape(1, -1))
			predict_label = int(predict_label[1][0][0])

			print(predict_label)

			if predict_label == 0:
				#print("Parking cell is empty!")
				result_list.append(0)
				cv2.line(one_park_image, point1, point2, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point3, point4, (255, 0, 0), 4)
				cv2.line(one_park_image, point4, point1, (255, 0, 0), 4)
			else:
				#print("Parking cell is occupied.")
				result_list.append(1)
				cv2.line(one_park_image, point1, point3, (255, 0, 0), 4)
				cv2.line(one_park_image, point2, point4, (255, 0, 0), 4)
				#cv2.rectangle(one_park_image, point1, point4, (0, 0, 255))
			
			if result_list[current_parking_spot] == int(gt_lines[current_parking_spot].strip()):
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)
				#cv2.fillPoly(one_park_image, pts = [np.array([point1, point2, point3, point4])], color= (0, 255, 0, 100))
			else:
				overlay = one_park_image.copy()
				cv2.fillPoly(overlay, pts = [np.array([point1, point2, point3, point4])], color= (0, 0, 255))
				one_park_image = cv2.addWeighted(overlay, 0.3, one_park_image, 1 - 0.3, 0)

			current_parking_spot += 1
		
		cv2.imshow('one_park_image', one_park_image)

		cv2.imshow('lbp_processed', lbp_processed)
		key = cv2.waitKey(0)
		if key == 27: # exit on ESC
			break


	false_positives = 0
	false_negatives = 0
	true_positives = 0
	true_negatives = 0

	i = 0
	for line in gt_lines:
		gt = int(line.strip())
		if( result_list[i] == 1 and gt == 0 ):
			false_positives += 1
		if( result_list[i] == 0 and gt == 1):
			false_negatives += 1
		if( result_list[i] == 1 and gt == 1 ):
			true_positives += 1
		if( result_list[i] == 0 and gt == 0):
			true_negatives += 1

		i += 1


	acc = (float)(true_positives + true_negatives) / (float)(false_positives + false_negatives + true_negatives + true_positives)
	print("Accuracy: ", (float)((int)(acc * 10000.0)) / 100 )

	precision = (float) (true_positives) / (float)(true_positives + false_positives)
	recall = (float) (true_positives) / (float)(true_positives + false_negatives)
	F_one = (2.0 * (float)(precision * recall)) / (float)(precision + recall)

	print("TP", true_positives)
	print("TN", true_negatives)

	print("FP", false_positives)
	print("FN", false_negatives)

	print("precision: ", precision)
	print("recall: ", recall)
	print("F1 Score: ", F_one)
	
def hog_visualize(opa):
	print("Computing HOG")
	image = data.astronaut()
	image = io.imread('./train_images/full/full_132.png')
	image = io.imread('./augment_images/full/full_342.png')

	fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
						cells_per_block=(1, 1), visualize=True, channel_axis=-1)

	fig, (ax1, ax2) = plt.subplots(2, 2, figsize=(8, 4), sharex=True, sharey=True)

	ax1.axis('off')
	ax1.imshow(image, cmap=plt.cm.gray)
	ax1.set_title('Input image')

	# Rescale histogram for better display
	hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))

	ax2.axis('off')
	ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
	ax2.set_title('Histogram of Oriented Gradients')
	plt.show()

if __name__ == "__main__":
   hog_visualize(sys.argv[1:])
